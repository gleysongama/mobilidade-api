# Mobilidade API

O objetivo macro do projeto é promover a integração com duas APIs externas ao projeto e disponibilizar uma APIs para criação e consulta de pontos de táxi. Mais detalhes do projetos estão no arquivo: BACKEND_-_Avaliacao_Tecnica.pdf

OBS: Ao clonar o projeto, mude para branch develop antes de executa-lo.

# Frameworks e tecnologias

- **Spring-boot**: utilizado para otimizar o tempo e aumentar a produtividade da codificação, já que ele simplifica o desenvolvimento de aplicações trazendo uma série de configurações embarcadas.
- **Spring-data-jpa**: utilizado para permitir a manipulação da camada java com banco de dados.
- **Spring-mvc**: utilizado para confecção da API a ser consumida.
- **Lombok**: utilizado para minimizar o tempo de desenvolvimento tornando transparente a utilização de funções de acesso a propriedades de classe.
- **Modelmapper**: utilizado para fazer parse entre DTOs e Entidades de classe.
- **Springfox**: necessário para utilização do Swagger tanto para entendimento do funcionamento da API quanto para realização de testes de leitura e escrita de dados na aplicação.
- **MySQL**: banco de dados utilizado para persistir e armazenar dados consumidos das integrações, assim como das manipulações do usuário na utilização da API.
- **Docker compose**: utilizado para criação de imagens e levantar todo o ecossistema da aplicação em containers.
- 

## Procedimento para utilização do sistema

- 1.  Na raiz do projeto abra o terminal e digite: docker-compose up (tudo será preparado para utilização).
- 2.  No navegador de sua escolha, acesse: http://localhost:8080/swagger-ui/index.html

