package br.com.gleysongama.mobilidade.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.mobilidade.dto.request.ItinerarioRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;
import br.com.gleysongama.mobilidade.service.ItinerarioService;

@RestController
@RequestMapping("/api/v1/itinerarios")
public class ItinerarioController {
	
    @Autowired
    private ItinerarioService itinerarioService;

    @GetMapping
    public ResponseEntity<List<ItinerarioResponse>> findAll() {
    	try {
	    	List<ItinerarioResponse> itinerarioResponse = itinerarioService.findAll();
	    	if(!Objects.isNull(itinerarioResponse)) {
	    		if(itinerarioResponse.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(itinerarioResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @PostMapping
    public ResponseEntity<ItinerarioResponse> create(@RequestBody ItinerarioRequest itinerarioRequest) {
    	try {
	    	ItinerarioResponse itinerarioResponse = itinerarioService.save(itinerarioRequest);
	    	if(!Objects.isNull(itinerarioResponse)) {
	    		return new ResponseEntity<>(itinerarioResponse, HttpStatus.CREATED);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
	    	return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @GetMapping("/{idLinha}")
    public ResponseEntity<ItinerarioResponse> findById(@PathVariable String idLinha) {
    	try {
	    	ItinerarioResponse itinerarioResponse = itinerarioService.findById(idLinha);
	    	if(!Objects.isNull(itinerarioResponse)) {
	    		return new ResponseEntity<>(itinerarioResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
    	} catch (Exception e) {
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @PutMapping("/{idLinha}")
    public ResponseEntity<ItinerarioResponse> update(@PathVariable String idLinha, @RequestBody ItinerarioRequest itinerarioRequest) {
    	try {
	    	ItinerarioResponse itinerarioResponse = itinerarioService.update(idLinha, itinerarioRequest);
	    	if(!Objects.isNull(itinerarioResponse)) {
	    		return new ResponseEntity<>(itinerarioResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
    	} catch (Exception e) {
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @DeleteMapping("/{idLinha}")
    public ResponseEntity<HttpStatus> delete(@PathVariable String idLinha) {
    	try {
    		itinerarioService.deleteById(idLinha);
    		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    	} catch (Exception e) {
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}