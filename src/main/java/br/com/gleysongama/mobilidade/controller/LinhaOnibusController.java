package br.com.gleysongama.mobilidade.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.mobilidade.dto.request.LinhaOnibusRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;
import br.com.gleysongama.mobilidade.dto.response.LinhaOnibusResponse;
import br.com.gleysongama.mobilidade.service.LinhaOnibusService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/linhas")
public class LinhaOnibusController {
	
    @Autowired
    private LinhaOnibusService linhaOnibusService;
    
    @GetMapping
    public ResponseEntity<List<LinhaOnibusResponse>> findAll() {    	
    	try {
	    	List<LinhaOnibusResponse> linhaOnibusResponse = linhaOnibusService.findAll();
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		if(linhaOnibusResponse.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping("/busca")
    public ResponseEntity<List<LinhaOnibusResponse>> findByNome(@RequestParam(value = "nome", defaultValue = "") String nome) {    	
    	try {
	    	List<LinhaOnibusResponse> linhaOnibusResponse = linhaOnibusService.findByNome(nome);
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		if(linhaOnibusResponse.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping("/linhas-por-raio")
    public ResponseEntity<List<ItinerarioResponse>> findByCoordenadasAndRaio(@RequestParam(value = "lat", defaultValue = "") String lat,
			    															  @RequestParam(value = "lng", defaultValue = "") String lng,
			    															  @RequestParam(value = "raio", defaultValue = "") String raio) {    	
    	try {
	    	List<ItinerarioResponse> linhaOnibusResponse = linhaOnibusService.findByCoordenadasAndRaio(lat, lng, raio);
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		if(linhaOnibusResponse.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @PostMapping
    public ResponseEntity<LinhaOnibusResponse> create(@RequestBody LinhaOnibusRequest linhaOnibusRequest) {
    	try {
    		LinhaOnibusResponse linhaOnibusResponse = linhaOnibusService.save(linhaOnibusRequest);
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.CREATED);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
	    	return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @GetMapping("/{idLinha}")
    public ResponseEntity<LinhaOnibusResponse> findById(@PathVariable String idLinha) {
    	try {
    		LinhaOnibusResponse linhaOnibusResponse = linhaOnibusService.findById(idLinha);
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @PutMapping("/{idLinha}")
    public ResponseEntity<LinhaOnibusResponse> update(@PathVariable String idLinha, @RequestBody LinhaOnibusRequest linhaOnibusRequest) {
    	try {
    		LinhaOnibusResponse linhaOnibusResponse = linhaOnibusService.update(idLinha, linhaOnibusRequest);
	    	if(!Objects.isNull(linhaOnibusResponse)) {
	    		return new ResponseEntity<>(linhaOnibusResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }
    
    @DeleteMapping("/{idLinha}")
    public ResponseEntity<HttpStatus> delete(@PathVariable String idLinha) {
    	try {
    		linhaOnibusService.deleteById(idLinha);
    		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}