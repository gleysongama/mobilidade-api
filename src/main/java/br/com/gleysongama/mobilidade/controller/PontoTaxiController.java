package br.com.gleysongama.mobilidade.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.mobilidade.dto.request.PontoTaxiRequest;
import br.com.gleysongama.mobilidade.dto.response.PontoTaxiResponse;
import br.com.gleysongama.mobilidade.service.PontoTaxiService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/ponto-taxi")
public class PontoTaxiController {
	
    @Autowired
    private PontoTaxiService pontoTaxiService;
    
    @PostMapping
    public ResponseEntity<PontoTaxiResponse> create(@RequestBody PontoTaxiRequest pontoTaxiRequest) {
    	try {
    		PontoTaxiResponse pontoTaxiResponse = pontoTaxiService.save(pontoTaxiRequest);
	    	if(!Objects.isNull(pontoTaxiResponse)) {
	    		return new ResponseEntity<>(pontoTaxiResponse, HttpStatus.CREATED);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
	    	return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping
    public ResponseEntity<List<PontoTaxiResponse>> findAll() {    	
    	try {
	    	List<PontoTaxiResponse> pontoTaxiResponses = pontoTaxiService.findAll();
	    	if(!Objects.isNull(pontoTaxiResponses)) {
	    		if(pontoTaxiResponses.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(pontoTaxiResponses, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping("/busca-por-nome")
    public ResponseEntity<PontoTaxiResponse> findByNome(@RequestParam(value = "nome", defaultValue = "") String nome) {    	
    	try {
	    	PontoTaxiResponse pontoTaxiResponse = pontoTaxiService.findByNome(nome);
	    	if(!Objects.isNull(pontoTaxiResponse)) {
	    		return new ResponseEntity<>(pontoTaxiResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping("/busca-por-coordenada")
    public ResponseEntity<PontoTaxiResponse> findByLatAndLng(@RequestParam(value = "lat", defaultValue = "") String lat,
			    											 @RequestParam(value = "lng", defaultValue = "") String lng) {    	
    	try {
	    	PontoTaxiResponse pontoTaxiResponse = pontoTaxiService.findByLatAndLng(lat, lng);
	    	if(!Objects.isNull(pontoTaxiResponse)) { 
	    		return new ResponseEntity<>(pontoTaxiResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @GetMapping("/busca-por-data-hora")
    public ResponseEntity<PontoTaxiResponse> findByDtCadastro(@RequestParam(value = "dateTime", defaultValue = "") String dateTime) {
    	try {
	    	PontoTaxiResponse pontoTaxiResponse = pontoTaxiService.findByDtCadastro(dateTime);
	    	if(!Objects.isNull(pontoTaxiResponse)) { 
	    		return new ResponseEntity<>(pontoTaxiResponse, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @GetMapping("/busca-por-data")
    public ResponseEntity<List<PontoTaxiResponse>> findByDate(@RequestParam(value = "date", defaultValue = "") String date) {
    	try {
    		List<PontoTaxiResponse> pontoTaxiResponses = pontoTaxiService.findByDate(date);
    		if(!Objects.isNull(pontoTaxiResponses)) {
	    		if(pontoTaxiResponses.isEmpty()) {
	    			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    		} 
	    		return new ResponseEntity<>(pontoTaxiResponses, HttpStatus.OK);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
    	} catch (Exception e) {
    		log.info("Erro 500: [{}].", e);
    		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}