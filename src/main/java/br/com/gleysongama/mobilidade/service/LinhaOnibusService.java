package br.com.gleysongama.mobilidade.service;

import java.util.List;

import br.com.gleysongama.mobilidade.dto.request.LinhaOnibusRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;
import br.com.gleysongama.mobilidade.dto.response.LinhaOnibusResponse;

public interface LinhaOnibusService {

	List<LinhaOnibusResponse> findAll();
	
	LinhaOnibusResponse findById(String idLinha);
	
	List<LinhaOnibusResponse> findByNome(String nome);
	
	LinhaOnibusResponse save(LinhaOnibusRequest linhaOnibusRequest);
	
	LinhaOnibusResponse update(String idLinha, LinhaOnibusRequest linhaOnibusRequest);
	
	List<ItinerarioResponse> findByCoordenadasAndRaio(String lat, String lng, String raio);
	
	void deleteById(String idLinha);
}
