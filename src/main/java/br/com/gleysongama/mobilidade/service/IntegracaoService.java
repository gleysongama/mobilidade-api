package br.com.gleysongama.mobilidade.service;

import java.util.List;

import br.com.gleysongama.mobilidade.dto.ItinerarioDTO;
import br.com.gleysongama.mobilidade.dto.LinhaOnibusDTO;

public interface IntegracaoService {

	List<LinhaOnibusDTO> findAllLinhaOnibus();
	
	ItinerarioDTO findItinerarioByLinhaOnibus(String idLinha);
}
