package br.com.gleysongama.mobilidade.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import br.com.gleysongama.mobilidade.dto.request.PontoTaxiRequest;
import br.com.gleysongama.mobilidade.dto.response.PontoTaxiResponse;
import br.com.gleysongama.mobilidade.service.PontoTaxiService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PontoTaxiServiceImpl implements PontoTaxiService {

	private File file;
	
	private List<String> listPontotaxi;
	
	private ModelMapper mapper = new ModelMapper();
	
	private List<PontoTaxiResponse> listPontotaxiResponse;
	
	public PontoTaxiServiceImpl() {
		loadFile();
	}
	
	@Override
	public PontoTaxiResponse save(PontoTaxiRequest request) {
		PontoTaxiResponse response = null;
		
		String line = request.getNome() + "#" + request.getLat() + "#" + request.getLng() + "#" + request.getDtCadastro();
		writeFile(line);
		response = mapper.map(request, PontoTaxiResponse.class);
		
		return response;
	}

	@Override
	public List<PontoTaxiResponse> findAll() {
		PontoTaxiResponse pontoTaxiResponse;
		listPontotaxiResponse = new ArrayList<PontoTaxiResponse>();
		
		for(String linha : readFile()) {
			StringTokenizer st = new StringTokenizer(linha);
			pontoTaxiResponse = new PontoTaxiResponse();
			
			pontoTaxiResponse.setNome(st.nextToken("#"));
			pontoTaxiResponse.setLat(st.nextToken("#"));
			pontoTaxiResponse.setLng(st.nextToken("#"));
			pontoTaxiResponse.setDtCadastro(LocalDateTime.parse(st.nextToken("#")));
			
			listPontotaxiResponse.add(pontoTaxiResponse);
		}
		return listPontotaxiResponse;
	}

	@Override
	public PontoTaxiResponse findByNome(String nome) {
		PontoTaxiResponse response = null;
		
		response = findAll().stream()
							.filter(pontoTaxi -> nome.equals(pontoTaxi.getNome()))
							.findAny()
							.orElse(new PontoTaxiResponse());
		
		return response;
	}

	@Override
	public PontoTaxiResponse findByLatAndLng(String lat, String lng) {
		PontoTaxiResponse response = null;
		
		response = findAll().stream()
				  				.filter(pontoTaxi -> lat.equals(pontoTaxi.getLat()) && lng.equals(pontoTaxi.getLng()))
								.findAny()
								.orElse(new PontoTaxiResponse());
		
		return response;
	}

	@Override
	public PontoTaxiResponse findByDtCadastro(String dateTime) {
		PontoTaxiResponse response = null;
		
		response = findAll().stream()
								.filter(pontoTaxi -> dateTime.equals(pontoTaxi.getDtCadastro().toString()))
								.findAny()
								.orElse(new PontoTaxiResponse());
		
		return response;
	}

	@Override
	public List<PontoTaxiResponse> findByDate(String date) {
		List<PontoTaxiResponse> responses = null;
		
		
		if(!Objects.isNull(date)) {
			responses = findAll().stream()
							 	 .filter(pontoTaxi -> date.equals(pontoTaxi.getDtCadastro().toLocalDate().toString()))
							 	 .collect(Collectors.toList());
		}
		
		return responses;
	}

	private void loadFile() {
		if(Objects.isNull(this.file)) {
			this.file = createFile();
			this.listPontotaxi = readFile();
			PontoTaxiResponse pontoTaxiResponse;
			listPontotaxiResponse = new ArrayList<PontoTaxiResponse>();
			
			for(String linha : listPontotaxi) {
				StringTokenizer st = new StringTokenizer(linha);
				pontoTaxiResponse = new PontoTaxiResponse();
				
				pontoTaxiResponse.setNome(st.nextToken("#"));
				pontoTaxiResponse.setLat(st.nextToken("#"));
				pontoTaxiResponse.setLng(st.nextToken("#"));
				pontoTaxiResponse.setDtCadastro(LocalDateTime.parse(st.nextToken("#")));
				
				listPontotaxiResponse.add(pontoTaxiResponse);
			}
		}
	}
	
	private File createFile() {
		File file = new File("pontos_de_taxi.txt");
		
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				log.info("Erro ao criar arquivo no diretório raiz do projeto: [{}]", e);
				e.printStackTrace();
			}
		}
		
		return file;
	}
		
	private File getFile() {
		if(this.file.exists()) {
			return this.file;
		} else {
			return null;
		}
	}
	
	private boolean writeFile(String fileLine) {		
		try {
			FileWriter fw = new FileWriter(getFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(fileLine);
			bw.newLine();
			bw.close();
			fw.close();
			return true;
		} catch (IOException e) {
			log.info("Erro ao escrever no arquivo, verifique se o mesmo existe no diretório: [{}]", e);
			e.printStackTrace();
		}
		return false;
	}

	private List<String> readFile() {
		List<String> lines = null;
		try {
			FileReader fr = new FileReader(getFile());
			BufferedReader br = new BufferedReader(fr);
			lines = new ArrayList<String>();
			
			while (br.ready()) {
				lines.add(br.readLine());
			}
			br.close();
			fr.close();
		} catch (IOException e) {
			log.info("Erro ao ler do arquivo, verifique se o mesmo existe no diretório: [{}]", e);
			e.printStackTrace();
		}
		return lines;
	}
}
