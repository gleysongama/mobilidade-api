package br.com.gleysongama.mobilidade.service;

import java.util.List;

import br.com.gleysongama.mobilidade.dto.request.PontoTaxiRequest;
import br.com.gleysongama.mobilidade.dto.response.PontoTaxiResponse;

public interface PontoTaxiService {

	PontoTaxiResponse save(PontoTaxiRequest pontoTaxiRequest);
	
	List<PontoTaxiResponse> findAll();
	
	PontoTaxiResponse findByNome(String nome);
	
	PontoTaxiResponse findByLatAndLng(String lat, String lng);
	
	PontoTaxiResponse findByDtCadastro(String dateTime);
	
	List<PontoTaxiResponse> findByDate(String date);
}
