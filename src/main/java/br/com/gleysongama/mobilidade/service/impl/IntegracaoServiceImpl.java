package br.com.gleysongama.mobilidade.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gleysongama.mobilidade.dto.ItinerarioDTO;
import br.com.gleysongama.mobilidade.dto.LinhaOnibusDTO;
import br.com.gleysongama.mobilidade.dto.PosicaoDTO;
import br.com.gleysongama.mobilidade.service.IntegracaoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IntegracaoServiceImpl implements IntegracaoService {

	@Value(value = "${poatransporte.url}")
	private String poatransporte;
	
	private final String URL_INTEGRACAO = "php/facades/process.php";
	
	@Autowired
	private RestTemplate restTemplate;
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public List<LinhaOnibusDTO> findAllLinhaOnibus() {
		log.info("Efetuando solicitação linhas de ônibus");
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("a", "nc");
		params.add("p", "%");
		params.add("t", "o");
		
		URI uri = UriComponentsBuilder.fromUriString(poatransporte)
									  .path(URL_INTEGRACAO)
									  .queryParams(params)
									  .build()
									  .encode()
									  .toUri();
		
		String response = restTemplate.getForObject(uri, String.class);
		List<LinhaOnibusDTO> listDto = converterLinhaOnibus(response);		
		
		log.info("Retorno solicitação linhas de ônibus: response: [{}].", listDto.toString());
		
		return listDto;
	}
	
	

	@Override
	public ItinerarioDTO findItinerarioByLinhaOnibus(String idLinha) {
		log.info("Efetuando solicitação itinerário da linha de ônibus: [{}]", idLinha);
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("a", "il");
		params.add("p", idLinha);
		
		URI uri = UriComponentsBuilder.fromUriString(poatransporte)
				  .path(URL_INTEGRACAO)
				  .queryParams(params)
				  .build()
				  .encode()
				  .toUri();

		String response = restTemplate.getForObject(uri, String.class);
		ItinerarioDTO itinerarioDTO = converterItinerario(response);
		
		log.info("Retorno da solicitação itinerário da linha de ônibus: [{}] - [{}]", idLinha, response);
		
		return itinerarioDTO;
	}
	
	// TODO - Converters
	
	private List<LinhaOnibusDTO> converterLinhaOnibus(String response) {
		List<LinhaOnibusDTO> listDto = null;
		
		try {
			LinhaOnibusDTO[] arrayDto = objectMapper.readValue(response, LinhaOnibusDTO[].class);
			listDto = new ArrayList<LinhaOnibusDTO>();
			for(LinhaOnibusDTO dto : arrayDto) {
				listDto.add(dto);
			}
		} catch (JsonMappingException e) {
			log.error("JsonMappingException ao converter linhas de ônibus: [{}].", e);
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ao converter linhas de ônibus: [{}].", e);
			e.printStackTrace();
		}
		
		return listDto;
	}
	
	private ItinerarioDTO converterItinerario(String response) {
		ItinerarioDTO itinerarioDTO = null;
		
		try {
			Map<String, Object> result = objectMapper.readValue(response, Map.class);
			
			itinerarioDTO = new ItinerarioDTO();
			
			Iterator<Entry<String, Object>> iterator = result.entrySet().iterator();
	        while (iterator.hasNext()) {
	            Entry<String, Object> entry = iterator.next();         
	            
	            if(entry.getKey().equalsIgnoreCase("idLinha")) {
					itinerarioDTO.setIdLinha(entry.getValue().toString());
				} else if(entry.getKey().equalsIgnoreCase("nome")) {
					itinerarioDTO.setNome(entry.getValue().toString());
				} else if(entry.getKey().equalsIgnoreCase("codigo")) {
					itinerarioDTO.setCodigo(entry.getValue().toString());
				} else {
					PosicaoDTO posicaoDTO = new PosicaoDTO();
					LinkedHashMap<String, String> linkedHashMap = (LinkedHashMap<String, String>) entry.getValue();
					
					posicaoDTO.setItinerarioDTO(itinerarioDTO);
					posicaoDTO.setLat(linkedHashMap.get("lat"));
					posicaoDTO.setLng(linkedHashMap.get("lng"));
					
					itinerarioDTO.getPosicoes().add(posicaoDTO);
				}
	        }			
		} catch (JsonMappingException e) {
			log.error("JsonMappingException ao converter itinerário de ônibus: [{}].", e);
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ao converter itinerário de ônibus: [{}].", e);
			e.printStackTrace();
		}
		
		return itinerarioDTO; 
	}

}
