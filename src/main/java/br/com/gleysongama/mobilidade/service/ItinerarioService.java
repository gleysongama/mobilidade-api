package br.com.gleysongama.mobilidade.service;

import java.util.List;

import br.com.gleysongama.mobilidade.dto.request.ItinerarioRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;

public interface ItinerarioService {

	List<ItinerarioResponse> findAll();
	
	ItinerarioResponse findById(String idLinha);
	
	ItinerarioResponse save(ItinerarioRequest itinerarioRequest);
	
	ItinerarioResponse update(String idLinha, ItinerarioRequest itinerarioRequest);
	
	void deleteById(String idLinha);
}
