package br.com.gleysongama.mobilidade.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.mobilidade.dto.LinhaOnibusDTO;
import br.com.gleysongama.mobilidade.dto.request.LinhaOnibusRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;
import br.com.gleysongama.mobilidade.dto.response.LinhaOnibusResponse;
import br.com.gleysongama.mobilidade.model.Itinerario;
import br.com.gleysongama.mobilidade.model.LinhaOnibus;
import br.com.gleysongama.mobilidade.model.Posicao;
import br.com.gleysongama.mobilidade.repository.LinhaOnibusRepository;
import br.com.gleysongama.mobilidade.repository.PosicaoRepository;
import br.com.gleysongama.mobilidade.service.IntegracaoService;
import br.com.gleysongama.mobilidade.service.LinhaOnibusService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LinhaOnibusServiceImpl implements LinhaOnibusService {
	
	@Autowired
	private LinhaOnibusRepository linhaOnibusRepository;
	
	@Autowired
	private PosicaoRepository posicaoRepository;
	
	@Autowired
	private IntegracaoService integracaoService;
	
	private ModelMapper mapper = new ModelMapper();

	@Override
	public List<LinhaOnibusResponse> findAll() {
		sincronizarDados();		
		List<LinhaOnibusResponse> response = null;
		response = linhaOnibusRepository.findAll()
										.stream()
										.map(linha -> mapper.map(linha, LinhaOnibusResponse.class))
										.collect(Collectors.toList());
		
		return response;
	}

	@Override
	public LinhaOnibusResponse findById(String idLinha) {
		LinhaOnibusResponse response = null;
		LinhaOnibus linhaOnibus = linhaOnibusRepository.findById(idLinha);
		
		if(!Objects.isNull(linhaOnibus)) {	
			response = mapper.map(linhaOnibus, LinhaOnibusResponse.class);
		}
		
		return response;
	}
	
	private LinhaOnibusResponse findById(Long id) {
		LinhaOnibusResponse response = null;
		Optional<LinhaOnibus> optional = linhaOnibusRepository.findById(id);
		
		if(optional.isPresent()) {	
			response = mapper.map(optional.get(), LinhaOnibusResponse.class);
		}
		
		return response;
	}
	
	@Override
	public List<LinhaOnibusResponse> findByNome(String nome) {
		List<LinhaOnibusResponse> response = null;
		response = linhaOnibusRepository.findByNome(nome)
										.stream()
										.map(linha -> mapper.map(linha, LinhaOnibusResponse.class))
										.collect(Collectors.toList());	
		
		return response;
	}
	
	private boolean validate(LinhaOnibusRequest linhaOnibusRequest) {
		LinhaOnibus linhaOnibus = null;
		
		linhaOnibus = linhaOnibusRepository.findById(linhaOnibusRequest.getId());
		
		return Objects.isNull(linhaOnibus);
	}

	@Override
	public LinhaOnibusResponse save(LinhaOnibusRequest linhaOnibusRequest) {
		LinhaOnibusResponse response = null;
		
		if(validate(linhaOnibusRequest)) {
			response = mapper.map(linhaOnibusRepository.save(mapper.map(linhaOnibusRequest, LinhaOnibus.class)), LinhaOnibusResponse.class);
		}
		
		return response;
	}
	
	@Override
	public LinhaOnibusResponse update(String idLinha, LinhaOnibusRequest linhaOnibusRequest) {
		LinhaOnibusResponse response = null;
		
		LinhaOnibusResponse linhaOnibusResponse = this.findById(idLinha);
		
		if(!Objects.isNull(linhaOnibusResponse)) {		
			LinhaOnibus linhaOnibus = mapper.map(linhaOnibusRequest, LinhaOnibus.class);	        
			linhaOnibus.setIdEntity(linhaOnibusResponse.getIdEntity());	        
	        response = mapper.map(linhaOnibusRepository.save(linhaOnibus), LinhaOnibusResponse.class);
		} else {
			this.save(linhaOnibusRequest);
		}
        
        return response;
	}
	
	@Override
	public void deleteById(String idLinha) {
		LinhaOnibusResponse linhaOnibusResponse = this.findById(idLinha);
		linhaOnibusRepository.deleteById(linhaOnibusResponse.getIdEntity());
	}
	
	@Override
	public List<ItinerarioResponse> findByCoordenadasAndRaio(String lat, String lng, String raio) {
		List<ItinerarioResponse> response = null;		
		List<Posicao> posicoes = posicaoRepository.findAll();
		
		List<Itinerario> itinerarios = calculaPontoCircunferencia(lat, lng, raio, posicoes);
		
		if(!Objects.isNull(itinerarios)) {
			response = itinerarios.stream()
								  .map(itinerario -> mapper.map(itinerario, ItinerarioResponse.class))
								  .collect(Collectors.toList());
		}
		
		return response;
	}
	
	private List<Itinerario> calculaPontoCircunferencia(String lat, String lng, String raio, List<Posicao> posicoes) {
		List<Itinerario> itinerarios = new ArrayList<Itinerario>();
		double x1 = Double.parseDouble(lat);
		double y1 = Double.parseDouble(lng);
		double r = Double.parseDouble(raio);		
		
		for(Posicao posicao : posicoes) {
			double x2 = Double.parseDouble(posicao.getLat());
			double y2 = Double.parseDouble(posicao.getLng());
			
			double ac = Math.abs(y2 - y1);
			double cb = Math.abs(x2 - x1);
			
			double z = Math.hypot(ac, cb);
			
			if(z <= r) {
				if(!itinerarios.contains(posicao)) {
					itinerarios.add(posicao.getItinerario());
				}					
				
			}
		}
		
		return itinerarios;
	}

	private void update(LinhaOnibusDTO linhaOnibusDTO) {		
		LinhaOnibus response = linhaOnibusRepository.findById(linhaOnibusDTO.getId());
		
		if(!Objects.isNull(response)) {
			log.info("Efetuando atualização de linha de ônibus: [{}] - [{}]", response.getId(), response.getNome());
			LinhaOnibus linhaOnibus = mapper.map(linhaOnibusDTO, LinhaOnibus.class);
			linhaOnibus.setIdEntity(response.getIdEntity());
	        linhaOnibusRepository.save(linhaOnibus);
		} else {
			LinhaOnibus linhaOnibus = mapper.map(linhaOnibusDTO, LinhaOnibus.class);
			linhaOnibus = linhaOnibusRepository.save(linhaOnibus);
			log.info("Criado linha de ônibus: [{}] - [{}]", linhaOnibus.getId(), linhaOnibus.getNome());			
		}
	}
	
	private void sincronizarDados() {
		List<LinhaOnibusDTO> linhaOnibusDTOs = integracaoService.findAllLinhaOnibus();
		
		for(LinhaOnibusDTO linhaOnibusDTO : linhaOnibusDTOs) {
			update(linhaOnibusDTO);
		}
	}
}
