package br.com.gleysongama.mobilidade.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.mobilidade.dto.ItinerarioDTO;
import br.com.gleysongama.mobilidade.dto.request.ItinerarioRequest;
import br.com.gleysongama.mobilidade.dto.response.ItinerarioResponse;
import br.com.gleysongama.mobilidade.model.Itinerario;
import br.com.gleysongama.mobilidade.repository.ItinerarioRepository;
import br.com.gleysongama.mobilidade.service.IntegracaoService;
import br.com.gleysongama.mobilidade.service.ItinerarioService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ItinerarioServiceImpl implements ItinerarioService {
	
	@Autowired
	private ItinerarioRepository itinerarioRepository;
	
	@Autowired
	private IntegracaoService integracaoService;
	
	private ModelMapper mapper = new ModelMapper();

	@Override
	public List<ItinerarioResponse> findAll() {
		List<ItinerarioResponse> response = null;
		response = itinerarioRepository.findAll()
									   .stream()
									   .map(linha -> mapper.map(linha, ItinerarioResponse.class))
									   .collect(Collectors.toList());

		return response;
	}

	@Override
	public ItinerarioResponse findById(String idLinha) {
		sincronizarDados(idLinha);
		ItinerarioResponse response = null;
		Itinerario itinerario = itinerarioRepository.findByIdLinha(idLinha);
		
		if(!Objects.isNull(itinerario)) {	
			response = mapper.map(itinerario, ItinerarioResponse.class);
		}
		
		return response;
	}
	
	
	@Override
	public ItinerarioResponse save(ItinerarioRequest itinerarioRequest) {
		ItinerarioResponse response = null;
		
		if(validate(itinerarioRequest)) {
			response = mapper.map(itinerarioRepository.save(mapper.map(itinerarioRequest, Itinerario.class)), ItinerarioResponse.class);
		}
		
		return response;
	}
	
	@Override
	public ItinerarioResponse update(String idLinha, ItinerarioRequest itinerarioRequest) {
		ItinerarioResponse response = null;
		
		ItinerarioResponse itinerarioResponse = this.findById(idLinha);
		
		if(!Objects.isNull(itinerarioResponse)) {
			Itinerario itinerario = mapper.map(itinerarioRequest, Itinerario.class);	        
			itinerario.setIdEntity(itinerarioResponse.getIdEntity());	        
	        response = mapper.map(itinerarioRepository.save(itinerario), ItinerarioResponse.class);
		} else {
			this.save(itinerarioRequest);
		}
		
        return response;
	}

	@Override
	public void deleteById(String idLinha) {
		ItinerarioResponse itinerarioResponse = this.findById(idLinha);
		itinerarioRepository.deleteById(itinerarioResponse.getIdEntity());
	}

	private ItinerarioResponse findById(Long id) {
		ItinerarioResponse response = null;
		Optional<Itinerario> optional = itinerarioRepository.findById(id);
		
		if(optional.isPresent()) {	
			response = mapper.map(optional.get(), ItinerarioResponse.class);
		}
		
		return response;
	}

	private boolean validate(ItinerarioRequest itinerarioRequest) {
		Itinerario itinerario = null;
		
		itinerario = itinerarioRepository.findByIdLinha(itinerarioRequest.getIdLinha());
		
		return Objects.isNull(itinerario);
	}
		
	private void update(ItinerarioDTO itinerarioDTO) {		
		Itinerario response = itinerarioRepository.findByIdLinha(itinerarioDTO.getIdLinha());
		
		if(!Objects.isNull(response)) {
			log.info("Efetuando atualização de linha de ônibus: [{}] - [{}]", response.getIdLinha(), response.getNome());
			Itinerario itinerario = mapper.map(itinerarioDTO, Itinerario.class);
			itinerario.setIdEntity(response.getIdEntity());
			itinerarioRepository.save(itinerario);
		} else {
			Itinerario itinerario = mapper.map(itinerarioDTO, Itinerario.class);
			itinerario = itinerarioRepository.save(itinerario);
			log.info("Criado linha de ônibus: [{}] - [{}]", itinerario.getIdLinha(), itinerario.getNome());			
		}
	}
	
	private void sincronizarDados(String idLinha) {
		ItinerarioDTO itinerarioDTO = integracaoService.findItinerarioByLinhaOnibus(idLinha);
		
		update(itinerarioDTO);
	}
}
