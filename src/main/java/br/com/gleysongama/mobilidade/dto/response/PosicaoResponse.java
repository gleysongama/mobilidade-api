package br.com.gleysongama.mobilidade.dto.response;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PosicaoResponse {
	
	private String lat;
	
	private String lng;	
}
