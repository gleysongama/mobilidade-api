package br.com.gleysongama.mobilidade.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PosicaoDTO {
	
	private String lat;
	
	private String lng;	
	
	private ItinerarioDTO itinerarioDTO;
}
