package br.com.gleysongama.mobilidade.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LinhaOnibusRequest {
	
	private String id;
	
	private String codigo;
	
	private String nome;
}
