package br.com.gleysongama.mobilidade.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinhaOnibusDTO {
	
	private String id;
	
	private String codigo;
	
	private String nome;
}
