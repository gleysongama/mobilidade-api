package br.com.gleysongama.mobilidade.dto.response;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LinhaOnibusResponse {
	
    private Long idEntity;
    
    private String id;
	
	private String codigo;
	
	private String nome;
}
