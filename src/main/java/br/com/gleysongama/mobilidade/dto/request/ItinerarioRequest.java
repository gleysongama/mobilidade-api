package br.com.gleysongama.mobilidade.dto.request;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ItinerarioRequest {
	
	private String idLinha;
	
	private String nome;
	
	private String codigo;	
	
	private List<PosicaoRequest> posicoes = new ArrayList<PosicaoRequest>();	
}
