package br.com.gleysongama.mobilidade.dto.request;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PontoTaxiRequest {
	
	private String nome;
	
	private String lat;
	
	private String lng;
	
	private LocalDateTime dtCadastro;
}
