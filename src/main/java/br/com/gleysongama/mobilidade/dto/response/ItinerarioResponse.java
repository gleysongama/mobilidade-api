package br.com.gleysongama.mobilidade.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ItinerarioResponse {
	
	private Long idEntity;
	
	private String idLinha;
	
	private String nome;
	
	private String codigo;	
	
	private List<PosicaoResponse> posicoes = new ArrayList<PosicaoResponse>();	
}
