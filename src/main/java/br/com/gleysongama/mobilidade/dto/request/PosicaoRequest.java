package br.com.gleysongama.mobilidade.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PosicaoRequest {
	
	private String lat;
	
	private String lng;	
}
