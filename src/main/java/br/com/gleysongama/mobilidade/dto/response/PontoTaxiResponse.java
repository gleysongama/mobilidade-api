package br.com.gleysongama.mobilidade.dto.response;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PontoTaxiResponse {
	
	private String nome;
	
	private String lat;
	
	private String lng;
	
	private LocalDateTime dtCadastro;
}
