package br.com.gleysongama.mobilidade.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ItinerarioDTO {
	
	private String idLinha;
	
	private String nome;
	
	private String codigo;	
	
	private List<PosicaoDTO> posicoes = new ArrayList<PosicaoDTO>();	
}
