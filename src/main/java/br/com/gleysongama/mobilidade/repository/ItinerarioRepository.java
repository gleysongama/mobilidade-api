package br.com.gleysongama.mobilidade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gleysongama.mobilidade.model.Itinerario;

public interface ItinerarioRepository extends JpaRepository<Itinerario, Long> {
    
	@Query("SELECT i FROM Itinerario i WHERE i.idLinha = ?1")
	public Itinerario findByIdLinha(String idLinha);
}