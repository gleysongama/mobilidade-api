package br.com.gleysongama.mobilidade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gleysongama.mobilidade.model.LinhaOnibus;

public interface LinhaOnibusRepository extends JpaRepository<LinhaOnibus, Long> {
    
	@Query("SELECT l FROM LinhaOnibus l WHERE l.id = ?1")
	public LinhaOnibus findById(String id);
	
	public List<LinhaOnibus> findByNome(String nome);
	
}