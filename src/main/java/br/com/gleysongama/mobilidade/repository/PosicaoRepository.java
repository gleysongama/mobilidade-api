package br.com.gleysongama.mobilidade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.mobilidade.model.Posicao;

public interface PosicaoRepository extends JpaRepository<Posicao, Long> {
    
}