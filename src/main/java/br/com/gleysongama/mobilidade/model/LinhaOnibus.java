package br.com.gleysongama.mobilidade.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
public class LinhaOnibus {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEntity;
	
	private String id;
	
	private String codigo;
	
	private String nome;
	
	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;
}
