package br.com.gleysongama.mobilidade.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
public class Itinerario {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEntity;
	
	private String idLinha;
	
	private String nome;
	
	private String codigo;	
	
	@OneToMany(mappedBy = "itinerario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Posicao> posicoes = new ArrayList<Posicao>();
	
	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;

	@Override
	public String toString() {
		return "Itinerario [idLinha=" + idLinha + ", nome=" + nome + ", codigo=" + codigo + "]";
	}
}
