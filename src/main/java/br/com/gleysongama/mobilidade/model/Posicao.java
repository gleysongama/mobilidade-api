package br.com.gleysongama.mobilidade.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
public class Posicao {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEntity;
	
	private String lat;
	
	private String lng;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Itinerario itinerario;	
	
	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;
}
